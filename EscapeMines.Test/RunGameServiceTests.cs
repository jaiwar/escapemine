﻿using EscapeMines.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using EscapeMines.Services;
using System.Linq;
using EscapeMines.Enums;
using EscapeMines.Models;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using Xunit;

namespace EscapeMines.Test
{
    public class RunGameServiceTests
    {
        public RunGameServiceTests()
        {
            var standardOut = new StreamWriter(Console.OpenStandardOutput()) { AutoFlush = true };
            Console.SetOut(standardOut);
        }

        [Fact]
        public void running_game_with_invalid_game_settings()
        {
            var fileReaderService = new Mock<IFileReaderService>();
            fileReaderService.Setup(x => x.ReadFileContents(It.IsAny<string>()))
                .Returns(new List<string>());

            var gameSettingsService = new Mock<IGameSettingsService>();
            gameSettingsService.Setup(x => x.ParseGameSettings(It.IsAny<IEnumerable<string>>()))
                .Returns(new GameSettings());

            var loadGameService = new Mock<ILoadGameService>();
            loadGameService.Setup(x => x.LoadGame(It.IsAny<GameSettings>())).Returns(new GameBoard());

            var runGameService = new RunGameService(fileReaderService.Object, gameSettingsService.Object, loadGameService.Object);

            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                runGameService.Play(It.IsAny<string>());
                Assert.Equal("Invalid Game Settings", sw.ToString().Trim());
            }
        }

        [Fact]
        public void running_game_with_invalid_game_board()
        {
            var fileReaderService = new Mock<IFileReaderService>();
            fileReaderService.Setup(x => x.ReadFileContents(It.IsAny<string>()))
                .Returns(new List<string> { "5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M"});

            var gameSettingsService = new Mock<IGameSettingsService>();
            gameSettingsService.Setup(x => x.ParseGameSettings(It.IsAny<IEnumerable<string>>()))
                .Returns(new GameSettings
            {
                BoardSize = new int[4, 5],
                ExitPoint = new Cell { X = 4, Y = 2 },
                Mines = new List<Cell> { new Cell { X = 1, Y = 1 }, new Cell { X = 1, Y = 3 }, new Cell { X = 3, Y = 3 } },
                StartPoint = new StartPoint { Cell = new Cell { X = 0, Y = 1 }, Direction = Direction.N },
                Sequences = new List<List<Moves>>
                {
                    new List<Moves> {Moves.M, Moves.R, Moves.M, Moves.M, Moves.M, Moves.M, Moves.R, Moves.M, Moves.M}
                }
            });

            var loadGameService = new Mock<ILoadGameService>();
            loadGameService.Setup(x => x.LoadGame(It.IsAny<GameSettings>())).Returns(new GameBoard());

            var runGameService = new RunGameService(fileReaderService.Object, gameSettingsService.Object, loadGameService.Object);

            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                runGameService.Play(It.IsAny<string>());
                Assert.Equal("Invalid Game Settings", sw.ToString().Trim());
            }
        }

        [Fact]
        public void running_game_with_valid_settings_and_board()
        {
            var fileReaderService = new Mock<IFileReaderService>();
            fileReaderService.Setup(x => x.ReadFileContents(It.IsAny<string>()))
                .Returns(new List<string> { "5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M", "R M M M", "L L M L M M M M", "M L M", "M R M M M" });

            var gameSettings = new GameSettings
            {
                BoardSize = new int[4, 5],
                ExitPoint = new Cell {X = 4, Y = 2},
                Mines = new List<Cell> {new Cell {X = 1, Y = 1}, new Cell {X = 1, Y = 3}, new Cell {X = 3, Y = 3}},
                StartPoint = new StartPoint {Cell = new Cell {X = 0, Y = 1}, Direction = Direction.N},
                Sequences = new List<List<Moves>>
                {
                    new List<Moves> { Moves.M, Moves.R, Moves.M, Moves.M, Moves.M, Moves.M, Moves.R, Moves.M, Moves.M },
                    new List<Moves> { Moves.R,Moves.M, Moves.M, Moves.M },
                    new List<Moves> { Moves.L,Moves.L, Moves.M, Moves.L, Moves.M, Moves.M, Moves.M, Moves.M },
                    new List<Moves> { Moves.M,Moves.L, Moves.M},
                    new List<Moves> { Moves.M,Moves.R, Moves.M, Moves.M , Moves.M , Moves.M }
                }
            };

            var gameSettingsService = new Mock<IGameSettingsService>();
            gameSettingsService.Setup(x => x.ParseGameSettings(It.IsAny<IEnumerable<string>>()))
                .Returns(gameSettings);

            var loadgame = new LoadGameService();
            var result = loadgame.LoadGame(gameSettings);

            var loadGameService = new Mock<ILoadGameService>();
            loadGameService.Setup(x => x.LoadGame(It.IsAny<GameSettings>())).Returns(result);

            var runGameService = new RunGameService(fileReaderService.Object, gameSettingsService.Object, loadGameService.Object);

            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                runGameService.Play(It.IsAny<string>());
                Assert.Equal("Sequence 1: Success!\r\nSequence 2: Mine hit!\r\nSequence 3: Success!\r\nSequence 4: Turtle hit the wall!\r\nSequence 5: Still in danger!\r\n", sw.ToString());
            }
        }
    }
}
