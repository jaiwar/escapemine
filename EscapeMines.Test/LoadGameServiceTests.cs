﻿using System;
using System.Collections.Generic;
using System.Text;
using EscapeMines.Models;
using EscapeMines.Services;
using EscapeMines.Enums;
using Xunit;

namespace EscapeMines.Test
{
    public class LoadGameServiceTests
    {
        private LoadGameService _loadGameService = new LoadGameService();

        [Fact]
        public void passing_Valid_GameSettings_return_board()
        {
            var gameSettings = new GameSettings
            {
                BoardSize = new int [4, 5],
                ExitPoint = new Cell {X = 4, Y = 2},
                Mines = new List<Cell> {new Cell {X = 1, Y = 1}, new Cell {X = 1, Y = 3}, new Cell {X = 3, Y = 3}},
                StartPoint = new StartPoint {Cell = new Cell {X = 0, Y = 1}, Direction = Direction.N},
                Sequences = new List<List<Moves>>
                {
                    new List<Moves> {Moves.M, Moves.R, Moves.M, Moves.M, Moves.M, Moves.M, Moves.R, Moves.M, Moves.M}
                }
            };
            var result = _loadGameService.LoadGame(gameSettings);

            //board Size
            Assert.Equal(5, result.Board.GetLength(0));
            Assert.Equal(4, result.Board.GetLength(1));

            //start point
            Assert.Equal(CellType.Entry, result.Board[0, 1].CellType);

            //exit point
            Assert.Equal(CellType.Exit, result.Board[4, 2].CellType);

            //mines
            Assert.Equal(CellType.Mine, result.Board[1, 1].CellType);
            Assert.Equal(CellType.Mine, result.Board[1, 3].CellType);
            Assert.Equal(CellType.Mine, result.Board[3, 3].CellType);
        }

        [Fact]
        public void passing_empty_GameSettings_return_null()
        {
            var gameSettings = new GameSettings();
            var result = _loadGameService.LoadGame(gameSettings);
            Assert.Null(result);
        }

        [Fact]
        public void passing_partial_game_settings_returns_null()
        {
            var gameSettings = new GameSettings()
            {
                BoardSize = new int[4, 5]
            };

            var result = _loadGameService.LoadGame(gameSettings);
            Assert.Null(result);
        }

    }
}
