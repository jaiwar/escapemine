﻿using System;
using System.Collections.Generic;
using System.Text;
using EscapeMines.Models;
using EscapeMines.Services;
using Xunit;

namespace EscapeMines.Test
{
    public class GameSettingsServiceTets
    {
        public GameSettingsService GameSettingsService = new GameSettingsService();

        //Empty parameter
        [Fact]
        public void Pass_Empty_string_to_GameSettings_Parser_returns_Null()
        {
            var result = GameSettingsService.ParseGameSettings(new List<string>());
            Assert.Null(result);
        }

        // board size is invalid
        [Theory]
        [InlineData("-1 -1", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M")]
        [InlineData("-1 0", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M")]
        [InlineData("1 ", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M")]
        public void Invalid_Board_Size_on_GameSettings_returns_Null(params string[] settingsStringList)
        {
            var result = GameSettingsService.ParseGameSettings(settingsStringList);
            Assert.Null(result);
        }

        // mines data is invalid
        [Theory]
        [InlineData("5 4", "1,1 1,3 3,-3", "4 2", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3 3", "4 2", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1", "4 2", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,7 6,1 5,5 8,8", "4 2", "0 1 N", "M R M M M M R M M")]
        public void Invalid_Mine_data_on_GameSettings_returns_Null(params string[] settingsStringList)
        {
            var result = GameSettingsService.ParseGameSettings(settingsStringList);
            Assert.Null(result);
        }

        // exit point is invalid
        [Theory]
        [InlineData("5 4", "1,1 1,3 3,3", "-4 -2", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 8", "0 1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "6 2", "0 1 N", "M R M M M M R M M")]
        public void Invalid_exit_point_on_GameSettings_returns_Null(params string[] settingsStringList)
        {
            var result = GameSettingsService.ParseGameSettings(settingsStringList);
            Assert.Null(result);
        }

        // start point is invalid
        [Theory]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 -1 N", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 J", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 1", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 ", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 7 W", "M R M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "7 1 S", "M R M M M M R M M")]
        public void Invalid_start_point_on_GameSettings_returns_Null(params string[] settingsStringList)
        {
            var result = GameSettingsService.ParseGameSettings(settingsStringList);
            Assert.Null(result);
        }

        // sequences are invalid
        [Theory]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M J M M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M 1 3 M M M R M M")]
        [InlineData("5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M, R M M M M R M M")]
        public void Sequences_on_GameSettings_returns_Null(params string[] settingsStringList)
        {
            var result = GameSettingsService.ParseGameSettings(settingsStringList);
            Assert.Null(result);
        }

        // All data is valid
        [Fact]
        public void Board_Size_is_Valid_on_GameSettings_returns_Settings()
        {
            var fileContents = new List<string> { "5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M", "R M M M" };
            var result = GameSettingsService.ParseGameSettings(fileContents);
            Assert.IsType<GameSettings>(result);
        }

        // No sequences found
        [Fact]
        public void No_sequences_found_on_GameSettings_returns_Settings()
        {
            var fileContents = new List<string> { "5 4", "1,1 1,3 3,3", "4 2", "0 1 N" };
            var result = GameSettingsService.ParseGameSettings(fileContents);
            Assert.Null(result);
        }
    }
}
