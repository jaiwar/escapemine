using System.Collections.Generic;
using System.Linq;
using EscapeMines.Services;
using Xunit;

namespace EscapeMines.Test
{
    public class FileReaderServiceTests
    {
        private readonly List<string> _fileContents = new List<string> { "5 4", "1,1 1,3 3,3", "4 2", "0 1 N", "M R M M M M R M M" };

        [Fact]
        public void Passing_Valid_FileName_And_File_Exists()
        {
            var fileReaderService = new FileReaderService();
            var result = fileReaderService.ReadFileContents("game-settings").ToList();

            Assert.Equal(_fileContents.Count, result.Count);
            Assert.Equal("5 4", result[0]);
            Assert.Equal("1,1 1,3 3,3", result[1]);
            Assert.Equal("4 2", result[2]);
            Assert.Equal("0 1 N", result[3]);
            Assert.Equal("M R M M M M R M M", result[4]);
        }

        [Theory]
        [InlineData("gameSettings")]
        [InlineData(" ")]
        [InlineData("game-settings.txt")]
        public void Passing_InValid_FileName_And_File_Does_Not_Exist(string filename)
        {
            var fileReaderService = new FileReaderService();
            var result = fileReaderService.ReadFileContents(filename).ToList();
            Assert.Empty(result);
        }
    }
}
