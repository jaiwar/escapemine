﻿using EscapeMines.Enums;

namespace EscapeMines.Models
{
    public class StartPoint
    {
        public Cell Cell { get; set; }
        public Direction Direction { get; set; }
    }
}
