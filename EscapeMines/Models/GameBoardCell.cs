﻿using EscapeMines.Enums;

namespace EscapeMines.Models
{
    public class GameBoardCell : Cell
    {
        public CellType CellType { get; set; }
        public bool TurtleInCell { get; set; }
    }
}