﻿using System.Collections.Generic;
using EscapeMines.Enums;

namespace EscapeMines.Models
{
    public class GameSettings
    {
        public int[,] BoardSize { get; set; }

        public StartPoint StartPoint { get; set; }

        public Cell ExitPoint { get; set; }

        public IEnumerable<Cell> Mines { get; set; }

        public IEnumerable<IEnumerable<Moves>> Sequences { get; set; }
    }
}