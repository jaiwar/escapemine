﻿using System;
using Microsoft.Extensions.DependencyInjection;
using EscapeMines.Interfaces;
using EscapeMines.Services;

namespace EscapeMines
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddTransient<IFileReaderService, FileReaderService>()
                .AddTransient<IGameSettingsService, GameSettingsService>()
                .AddTransient<ILoadGameService, LoadGameService>()
                .AddTransient<IRunGameService, RunGameService>()
                .BuildServiceProvider();

            //var settingsFileName = args[0];
            var settingsFileName = "game-settings";

            var runGameService = serviceProvider.GetService<IRunGameService>();
            runGameService.Play(settingsFileName);

            Console.ReadLine();
        }
    }
}
