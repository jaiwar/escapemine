﻿namespace EscapeMines.Enums
{
    public enum CellType
    {
        None,
        Entry,
        Exit,
        Mine
    }
}