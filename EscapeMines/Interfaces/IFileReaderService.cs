﻿using System.Collections.Generic;

namespace EscapeMines.Interfaces
{
    public interface IFileReaderService
    {
        IEnumerable<string> ReadFileContents(string fileName);
    }
}