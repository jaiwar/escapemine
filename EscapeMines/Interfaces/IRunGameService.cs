﻿using System.Collections.Generic;
using EscapeMines.Enums;
using EscapeMines.Models;

namespace EscapeMines.Interfaces
{
    public interface IRunGameService
    {
        //void MoveTurtle(GameBoard board, IEnumerable<Moves> sequence, GameSettings gameSettings, int sequenceNumber);
        void Play(string settingsFileName);
    }
}
