﻿using System.Collections.Generic;
using EscapeMines.Models;

namespace EscapeMines.Interfaces
{
    public interface ILoadGameService
    {
        GameBoard LoadGame(GameSettings gameSettings);
        //GameBoard CreateGameBoard(int x, int y);
        //void AddEntryPoint(GameBoard gameBoard, StartPoint startPoint); // add turtle too
        //void AddExitPoint(GameBoard gameBoard, Cell exitPoint);
        //void AddMines(GameBoard gameBoard, IEnumerable<Cell> mines);
    }
}