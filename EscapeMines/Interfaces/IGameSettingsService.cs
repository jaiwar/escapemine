﻿using System.Collections.Generic;
using EscapeMines.Models;

namespace EscapeMines.Interfaces
{
    public interface IGameSettingsService
    {
        GameSettings ParseGameSettings(IEnumerable<string> gameSettings);
    }
}