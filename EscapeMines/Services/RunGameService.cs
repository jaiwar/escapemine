﻿using System;
using System.Collections.Generic;
using System.Linq;
using EscapeMines.Enums;
using EscapeMines.Interfaces;
using EscapeMines.Models;

namespace EscapeMines.Services
{
    public class RunGameService : IRunGameService
    {
        private readonly IFileReaderService _fileReaderService;
        private readonly IGameSettingsService _gameSettingsService;
        private readonly ILoadGameService _loadGameService;

        public RunGameService(IFileReaderService fileReaderService, IGameSettingsService gameSettingsService,
            ILoadGameService loadGameService)
        {
            _fileReaderService = fileReaderService;
            _gameSettingsService = gameSettingsService;
            _loadGameService = loadGameService;
        }

        public void Play(string settingsFileName)
        {
            var gameSettingsString = _fileReaderService.ReadFileContents(settingsFileName);
            var gameSettings = _gameSettingsService.ParseGameSettings(gameSettingsString);
            if (gameSettings != null)
            {
                var gameBoard = _loadGameService.LoadGame(gameSettings);

                if (gameBoard?.Board == null || gameSettings.Sequences == null)
                {
                    Console.WriteLine("Invalid Game Settings");
                    return;
                }

                foreach (var sequence in gameSettings.Sequences.Select((value, index) => new { index, value }))
                {
                    MoveTurtle(gameBoard, sequence.value, gameSettings, sequence.index + 1);
                }
            }
            else
            {
                Console.WriteLine("Invalid Game Settings");
            }
        }

        private void MoveTurtle(GameBoard gameBoard, IEnumerable<Moves> sequence, GameSettings gameSettings, int sequenceNumber)
        {
            var startPosition = gameSettings.StartPoint;
            var direction = startPosition.Direction;
            Cell turtlePosition = new GameBoardCell { X = startPosition.Cell.X, Y = startPosition.Cell.Y };

            foreach (var move in sequence)
            {
                if (move == Moves.M)
                {
                    turtlePosition = Move(turtlePosition.X, turtlePosition.Y, direction);

                    if ((turtlePosition.X >= 0 && turtlePosition.X < gameSettings.BoardSize.GetLength(1)) && (turtlePosition.Y >= 0 && turtlePosition.Y < gameSettings.BoardSize.GetLength(0)))
                    {
                        var cell = gameBoard.Board[turtlePosition.X, turtlePosition.Y];

                        switch (cell.CellType)
                        {
                            case CellType.Mine:
                                Console.WriteLine($"Sequence {sequenceNumber}: Mine hit!");
                                return;
                            case CellType.Exit:
                                Console.WriteLine($"Sequence {sequenceNumber}: Success!");
                                return;
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Sequence {sequenceNumber}: Turtle hit the wall!");
                        return;
                    }
                }
                else
                {
                    direction = Rotate(move, direction);
                }

            }

            Console.WriteLine($"Sequence {sequenceNumber}: Still in danger!");
        }

        private Cell Move(int x, int y, Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    y = y - 1;
                    break;
                case Direction.S:
                    y = y + 1;
                    break;
                case Direction.E:
                    x = x + 1;
                    break;
                case Direction.W:
                    x = x - 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            return new Cell { X = x, Y = y };
        }

        private Direction Rotate(Moves rotate, Direction direction)
        {
            switch (direction)
            {
                case Direction.N:
                    return rotate == Moves.L ? Direction.W : Direction.E;
                case Direction.S:
                        return rotate == Moves.L ? Direction.E : Direction.W;
                case Direction.E:
                    return rotate == Moves.L ? Direction.N : Direction.S;
                case Direction.W:
                        return rotate == Moves.L ? Direction.S : Direction.N;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}
