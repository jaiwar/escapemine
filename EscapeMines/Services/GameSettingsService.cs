﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using EscapeMines.Enums;
using EscapeMines.Interfaces;
using EscapeMines.Models;

namespace EscapeMines.Services
{
    public class GameSettingsService : IGameSettingsService
    {
        private int[] _boardSize;

        public GameSettings ParseGameSettings(IEnumerable<string> gameSettings)
        {
            if (gameSettings.Any())
            {
                var gameSettingsArray = gameSettings.ToArray();

                var boardSize = GetBoardSize(gameSettingsArray[0]);
                if (boardSize == null)
                    return null;
                _boardSize = new [] {boardSize.GetLength(0), boardSize.GetLength(1)};

                var mines = GetMinesList(gameSettingsArray[1]);
                var exitPoint = GetExitPoint(gameSettingsArray[2]);
                var startPoint = GetStartPoint(gameSettingsArray[3]);
                var sequences = GetSequences(gameSettings.Skip(4));

                if (mines == null || exitPoint == null || startPoint == null || sequences == null)
                {
                    return null;
                }

                return new GameSettings
                {
                    BoardSize = boardSize,
                    ExitPoint = exitPoint,
                    Mines = mines,
                    StartPoint = startPoint,
                    Sequences = sequences
                };
            }

            return null;
        }

        private int[,] GetBoardSize(string values)
        {
            if (string.IsNullOrEmpty(values))
                return null;

            var boardSize = values.Trim().Split(" ");

            if (boardSize.Length != 2)
                return null;

            var x = Convert.ToInt32(boardSize[0]);
            var y = Convert.ToInt32(boardSize[1]);

            return x >= 0 && y >= 0
                ? new int[y, x]
                : null;
        }

        private StartPoint GetStartPoint(string values)
        {
            if (string.IsNullOrEmpty(values))
                return null;

            var startPoint = values.Trim().Split(" ").Select(p => p.Trim()).ToArray();

            if (startPoint.Length != 3)
                return null;

            var x = Convert.ToInt32(startPoint[0]);
            var y = Convert.ToInt32(startPoint[1]);

            if (IsValidCoordinate(x, y) && Enum.TryParse(startPoint[2], true, out Direction direction) &&
                Enum.IsDefined(typeof(Direction), startPoint[2]))
            {
                return new StartPoint
                {
                    Cell = new Cell
                    {
                        X = x,
                        Y = y
                    },
                    Direction = direction
                };
            }

            return null;
        }

        private Cell GetExitPoint(string values)
        {
            if (string.IsNullOrEmpty(values))
                return null;

            var exitPoint = values.Trim().Split(" ");

            if (exitPoint.Length != 2)
                return null;

            var x = Convert.ToInt32(exitPoint[0]);
            var y = Convert.ToInt32(exitPoint[1]);

            return IsValidCoordinate(x, y)
                ? new Cell { X = x, Y = y }
                : null;
        }

        private List<Cell> GetMinesList(string values)
        {
            if (string.IsNullOrEmpty(values))
                return null;

            var mines = values.Trim().Split(" ");
            var minesList = new List<Cell>();
            if (mines.Length >= 1)
            {
                foreach (var minePoint in mines)
                {
                    var coordiantes = minePoint.Split(",");

                    if (coordiantes.Length != 2)
                        return null;

                    var x = Convert.ToInt32(coordiantes[0]);
                    var y = Convert.ToInt32(coordiantes[1]);

                    if (!IsValidCoordinate(x, y))
                        return null;

                    minesList.Add(new Cell
                    {
                        X = x,
                        Y = y
                    });
                }

                return minesList;
            }

            return null;
        }

        private IEnumerable<IEnumerable<Moves>> GetSequences(IEnumerable<string> sequencesList)
        {
            var seqList = new List<List<Moves>>();
            if (sequencesList.Any())
            {
                foreach (var sequence in sequencesList)
                {
                    var movesList = new List<Moves>();
                    var moves = sequence.ToArray().Where(x =>  x != ' ');

                    foreach (var move in moves)
                    {
                        if (Enum.IsDefined(typeof(Moves), move.ToString()) && Enum.TryParse(move.ToString(), true, out Moves mv))
                            movesList.Add(mv);
                        else
                            return null;
                    }

                    seqList.Add(movesList);
                }

                return seqList;
            }
            else
            {
                Console.WriteLine("No sequences found");
                return null;
            }
        }

        private bool IsValidCoordinate(int x, int y)
        {
            return (x >= 0 && x < _boardSize[1]) && (y >= 0 && y < _boardSize[0]);
        }
    }
}

