﻿using System;
using System.Collections.Generic;
using System.IO;
using EscapeMines.Interfaces;

namespace EscapeMines.Services
{
    public class FileReaderService : IFileReaderService
    {
        private const string FileExtension = ".txt";

        public IEnumerable<string> ReadFileContents(string fileName)
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName + FileExtension);

            if (File.Exists(filePath))
            {
                using (var fileStream = File.OpenRead(filePath))
                using (var streamReader = new StreamReader(fileStream))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
            else
            {
                Console.WriteLine($"Game Settings file {fileName}{FileExtension} does not exist");
            }
        }
    }
}
