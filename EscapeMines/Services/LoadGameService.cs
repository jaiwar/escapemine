﻿using System;
using System.Collections.Generic;
using EscapeMines.Enums;
using EscapeMines.Interfaces;
using EscapeMines.Models;

namespace EscapeMines.Services
{
    public class LoadGameService : ILoadGameService
    {
        public GameBoard LoadGame(GameSettings gameSettings)
        {
            try
            {
                var newGameBoard =
                    CreateGameBoard(gameSettings.BoardSize.GetLength(1), gameSettings.BoardSize.GetLength(0));

                AddEntryPoint(newGameBoard, gameSettings.StartPoint);
                AddExitPoint(newGameBoard, gameSettings.ExitPoint);
                AddMines(newGameBoard, gameSettings.Mines);

                return newGameBoard;
            }
            catch
            {
                return null;
            }
        }

        private GameBoard CreateGameBoard(int x, int y)
        {
            var gameBoard = new GameBoardCell[x, y];

            for (var yAxis = 0; yAxis < y; yAxis++)
            {
                for (var xAxis = 0; xAxis < x; xAxis++)
                {
                    gameBoard[xAxis, yAxis] = new GameBoardCell
                    {
                        X = xAxis,
                        Y = yAxis,
                        CellType = CellType.None,
                        TurtleInCell = false
                    };
                }
            }

            return new GameBoard {Board = gameBoard};
        }

        private void AddEntryPoint(GameBoard gameBoard, StartPoint startPoint)
        {
            gameBoard.Board[startPoint.Cell.X, startPoint.Cell.Y] = new GameBoardCell
            {
                CellType = CellType.Entry,
                TurtleInCell = true
            };
        }

        private void AddExitPoint(GameBoard gameBoard, Cell exitPoint)
        {
            gameBoard.Board[exitPoint.X, exitPoint.Y] = new GameBoardCell
            {
                CellType = CellType.Exit
            };
        }

        private void AddMines(GameBoard gameBoard, IEnumerable<Cell> mines)
        {
            foreach (var mine in mines)
            {
                gameBoard.Board[mine.X, mine.Y] = new GameBoardCell
                {
                    CellType = CellType.Mine
                };
            }
        }
    }
}
